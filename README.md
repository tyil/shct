# SHell Compatibility Tester

This utility allows for quickly running a given script or command on a number
of shells, in order to test whether it behaves the same in all of them.

## Installation

### Package Manager

If you're using [Gentoo](https://gentoo.org/), you're in luck! `shct` is
available from the [`sk-overlay`](https://gitlab.com/skitties/overlay)!

Add the overlay using a `repos.conf` entry, or using
[`layman`](https://wiki.gentoo.org/wiki/Layman). Once synced, install the
program using `emerge`.

    emerge -a app-shells/shct

### Manual

The utility itself is only a single POSIX compatible shell script. To install
it, simply copy it to a directory into your path, and make it executable.

    cp bin/shct /usr/bin/
    chmod +x !$/shct

### Installation suggestions

If set up, `shct` is able to provide installation instructions when `-v` is
supplied. For this to work, the `INSTALL_CMD_DIR` has to be set to a path
containing a file with the same name as the distribution name, in lowercase
letters. The distribution name is found in `/etc/*-release` (A different
release file exists per distribution), on the `NAME=` line.

## Usage

The program outputs usage information when invoked improperly. When running as
intended, it will output a list of tested shells, the number of bytes read from
STDOUT, the number of bytes read from STDERR and the exitcode of that
particular shell.

If the program was given `-v` as option, it will print a message for every
shell supported, but not available in the user's `$PATH`. If installation
suggestions have been set up, installation instructions for that particular
shell will be printed as well.

There are two distinct ways to use the program, using `-c <cmd>`, or by
supplying a path. When both are supplied, `-c` will take precedence and the
given file is ignored.

### Examples

**Using `-c`**

    $ shct -c 'echo $$'
    SHELL  OUT  ERR  EXIT
     bash    6    0     0
     dash    6    0     0
     fish    0  152   127
      zsh    6    0     0

**Using a path to a script**

    $ cat /tmp/example
    echo $$
    $ shct -v /tmp/example
    SHELL  OUT  ERR  EXIT
     bash    6    0     0
     dash    6    0     0
     fish    0  138   127
      zsh    6    0     0


## Copyright

This software is released under the terms of the GNU Aferro General Public
License, version 3 or later.
